package ru.t1.akolobov.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.model.IWBS;
import ru.t1.akolobov.tm.comparartor.CreatedComparator;
import ru.t1.akolobov.tm.comparartor.NameComparator;
import ru.t1.akolobov.tm.comparartor.StatusComparator;

import java.util.Comparator;

public enum Sort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE);

    @NotNull
    private final String displayName;

    @NotNull
    private final Comparator<? super IWBS> comparator;

    Sort(@NotNull String displayName, @NotNull Comparator<? super IWBS> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @Nullable
    public static Sort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final Sort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @NotNull
    public Comparator<? super IWBS> getComparator() {
        return comparator;
    }

}
